var candlestickVis = function() {
    var xScale = null;
    var yScale = null;
    var chartBody = null;
    var newCandlesticks = {
        drawCandlesticks: function drawChart(stock) {
            console.log("drawing chart for: " , stock)

            var width = 1000;
            var height = 625;
            var vheight = 200;
            d3.selectAll("svg").remove();
            d3.select(".card-body-area")
                .append("svg")
                .attr("id", "candlestick")
                .attr("class", "chart candlechart")
                .attr("width", width)
                .attr("height", height);
              
            d3.select(".card-body-area")
                .append("svg")
                .attr("id", "volume")
                .attr("class", "chart candlechart")
                .attr("width", width)
                .attr("height", vheight);

            d3.csv("data/" + stock).then(function(prices) {

                const months = {0 : 'Jan', 1 : 'Feb', 2 : 'Mar', 3 : 'Apr', 4 : 'May', 5 : 'Jun', 6 : 'Jul', 7 : 'Aug', 8 : 'Sep', 9 : 'Oct', 10 : 'Nov', 11 : 'Dec'}

                var dateFormat = d3.timeParse("%Q");
                for (var i = 0; i < prices.length; i++) {
                    prices[i]['datetime'] = dateFormat(prices[i]['datetime'])
                    prices[i]['open'] = +prices[i]['open']
                    prices[i]['close'] = +prices[i]['close']
                    prices[i]['high'] = +prices[i]['high']
                    prices[i]['low'] = +prices[i]['low']
                    prices[i]['volume'] = +prices[i]['volume']
                }

                const margin = {top: 15, right: 65, bottom: 50, left: 60},
                w = width - margin.left - margin.right,
                h = height - margin.top - margin.bottom;
                vh = vheight - margin.top - margin.bottom;

                var svg = d3.select("#candlestick")
//                                .attr("width", w + margin.left + margin.right)
//                                .attr("height", h + margin.top + margin.bottom)
                                .append("g")
                                .attr("transform", "translate(" +margin.left+ "," +margin.top+ ")");
                var volumeSvg = d3.select("#volume")
//                                .attr("width", w + margin.left + margin.right)
//                                .attr("height", vh + margin.top + margin.bottom)
                                .append("g")
                                .attr("transform", "translate(" +margin.left+ "," +margin.top+ ")");

                let dates = _.map(prices, 'datetime');

                var xmin = d3.min(prices.map(r => r.datetime.getTime()));
                var xmax = d3.max(prices.map(r => r.datetime.getTime()));
                xScale = d3.scaleLinear().domain([-1, dates.length])
                                .range([0, w])
                var xDateScale = d3.scaleQuantize().domain([0, dates.length]).range(dates)
                let xBand = d3.scaleBand().domain(d3.range(-1, dates.length)).range([0, w]).padding(0.3)
                var xAxis = d3.axisBottom()
                                        .scale(xScale)
                                        .tickFormat(function(d) {
                                              d = dates[d]
                                                hours = d.getHours()
                                                minutes = (d.getMinutes()<10?'0':'') + d.getMinutes() 
                                                amPM = hours < 13 ? 'am' : 'pm'
                                                return d.getDate() + ' ' + months[d.getMonth()] + ' ' + d.getFullYear()
                                            });
                
                var prettyDate = function(d) {
                    d = dates[d]
                                                hours = d.getHours()
                                                minutes = (d.getMinutes()<10?'0':'') + d.getMinutes() 
                                                amPM = hours < 13 ? 'am' : 'pm'
                                                return d.getDate() + ' ' + months[d.getMonth()] + ' ' + d.getFullYear()
                }

                svg.append("rect")
                            .attr("id","rect")
                            .attr("width", w)
                            .attr("height", h)
                            .style("fill", "none")
                            .style("pointer-events", "all")
                            .attr("clip-path", "url(#clip)")
                volumeSvg.append("rect")
                            .attr("id","rect")
                            .attr("width", w)
                            .attr("height", vh)
                            .style("fill", "none")
                            .style("pointer-events", "all")
                            .attr("clip-path", "url(#clip)")

                var gX = svg.append("g")
                            .attr("class", "axis x-axis") //Assign "axis" class
                            .attr("transform", "translate(0," + h + ")")
                            .call(xAxis)

                var volumeGx = volumeSvg.append("g")
                            .attr("class", "axis x-axis") //Assign "axis" class
                            .attr("transform", "translate(0," + vh + ")")
                            .call(xAxis)

                gX.selectAll(".tick text")
                  .call(wrap, xBand.bandwidth())

                volumeGx.selectAll(".tick text")
                  .call(wrap, xBand.bandwidth())

                var ymin = d3.min(prices.map(r => r.low));
                var ymax = d3.max(prices.map(r => r.high));
                var vmin = d3.min(prices.map(r => r.volume))
                var vmax = d3.max(prices.map(r => r.volume))
                var vScale = d3.scaleLinear().domain([vmin, vmax]).range([vh, 0]).nice();
                yScale = d3.scaleLinear().domain([ymin, ymax]).range([h, 0]).nice();
                var yAxis = d3.axisLeft()
                              .scale(yScale)
                var vyAxis = d3.axisLeft()
                              .scale(vScale)
                              .ticks(6)

                var gY = svg.append("g")
                            .attr("class", "axis y-axis")
                            .call(yAxis);
                var gVY = volumeSvg.append("g")
                            .attr("calss", "axis y-axis")
                            .call(vyAxis)

                chartBody = svg.append("g")
                            .attr("class", "chartBody")
                            .attr("clip-path", "url(#clip)");

                var volumeChartBody = volumeSvg.append("g")
                            .attr("class", "chartBody")
                            .attr("clip-path", "url(#clip)");

                // draw rectangles
                let candles = chartBody.selectAll(".candle")
                   .data(prices)
                   .enter()
                   .append("rect")
                   .attr('x', (d, i) => xScale(i) - xBand.bandwidth())
                   .attr("class", "candle")
                   .attr('y', d => yScale(Math.max(d.open, d.close)))
                   .attr('width', xBand.bandwidth())
                   .attr('height', d => (d.open === d.close) ? 1 : yScale(Math.min(d.open, d.close))-yScale(Math.max(d.open, d.close)))
                   .style("fill", d => (d.open === d.close) ? "silver" : (d.open > d.close) ? "red" : "green")
                   .on("click", function(d, i) {
                       console.log("clicked! " + d.open)
                       document.getElementById("date-value").innerHTML = prettyDate(i)
                       document.getElementById("open-value").innerHTML = "$" + d.open;
                       document.getElementById("close-value").innerHTML = "$" + d.close;
                       document.getElementById("high-value").innerHTML = "$" + d.high;
                       document.getElementById("low-value").innerHTML = "$" + d.low;
                   });

                // draw high and low
                let stems = chartBody.selectAll("g.line")
                   .data(prices)
                   .enter()
                   .append("line")
                   .attr("class", "stem")
                   .attr("x1", (d, i) => xScale(i) - xBand.bandwidth()/2)
                   .attr("x2", (d, i) => xScale(i) - xBand.bandwidth()/2)
                   .attr("y1", d => yScale(d.high))
                   .attr("y2", d => yScale(d.low))
                   .attr("stroke", d => (d.open === d.close) ? "white" : (d.open > d.close) ? "red" : "green")
                   .on("click", function(d, i) {
                       console.log("clicked! " + d.open)
                       document.getElementById("date-value").innerHTML = prettyDate(i)

                       document.getElementById("open-value").innerHTML = "$" + d.open;
                       document.getElementById("close-value").innerHTML = "$" + d.close;
                       document.getElementById("high-value").innerHTML = "$" + d.high;
                       document.getElementById("low-value").innerHTML = "$" + d.low;
                   });

                let volumeBars = volumeChartBody.selectAll(".volume")
                    .data(prices)
                    .enter()
                    .append("rect")
                    .attr('x', (d, i) => xScale(i) - xBand.bandwidth())
                    .attr('class', 'volume')
                    .attr('y', d => vScale(d.volume))
                    .attr('width', xBand.bandwidth())
                    .attr('height', d => vh - vScale(d.volume))
                    .attr('fill', 'silver')

                svg.append("defs")
                   .append("clipPath")
                   .attr("id", "clip")
                   .append("rect")
                   .attr("width", w)
                   .attr("height", h)

                const extent = [[0, 0], [w, h]];

                var resizeTimer;
                var zoom = d3.zoom()
                  .scaleExtent([1, 100])
                  .translateExtent(extent)
                  .extent(extent)
                  .on("zoom", zoomed)
                  .on('zoom.end', zoomend);

                svg.call(zoom)
                volumeSvg.call(zoom)

                var mouseG = svg.append("g")
                                .attr("class", "mouse-over-effects");
                var volumeMouseG = volumeSvg.append("g")
                                    .attr("class", "mouse-over-effects")


              // create a tooltip
//            var Tooltip = d3.select("#div_template")
//                .append("div")
//                .style("opacity", 0)
//                .attr("class", "tooltip")
//                .style("background-color", "white")
//                .style("border", "solid")
//                .style("border-width", "2px")
//                .style("border-radius", "5px")
//                .style("padding", "5px")
//
//            mouseG.append("path") // this is the black vertical line to follow mouse
//                  .attr("class", "mouse-line")
//                  .style("stroke", "black")
//                  .style("stroke-width", "1px")
//                  .style("opacity", "0")
//
//            mouseG.append("div")
//                .style("opacity", 0)
//                .attr("class", "tooltip")
//                .style("background-color", "white")
//                .style("border", "solid")
//                .style("border-width", "2px")
//                .style("border-radius", "5px")
//                .style("padding", "5px")
//
//            volumeMouseG.append("path") // this is the black vertical line to follow mouse
//                  .attr("class", "mouse-line")
//                  .style("stroke", "black")
//                  .style("stroke-width", "1px")
//                  .style("opacity", "0");
//
//            var lines = document.getElementsByClassName('candle');
//
//            var mousePerLine = mouseG.selectAll('.mouse-per-line')
//              .data(prices)
//              .enter()
//              .append("g")
//              .attr("class", "mouse-per-line")
//
//            mousePerLine.append("text")
//              .attr("transform", "translate(10,3)");
//
//            mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
//              .attr('width', w) // can't catch mouse events on a g element
//              .attr('height', h)
//              .attr('fill', 'none')
//              .attr('pointer-events', 'all')
//              .on('mouseout', function(d) { // on mouse out hide line, circles and text
//                d3.select(".mouse-line")
//                  .style("opacity", "0");
//                d3.selectAll(".mouse-per-line circle")
//                  .style("opacity", "0");
//                d3.selectAll(".mouse-per-line text")
//                  .style("opacity", "0");
//                d3.selectAll(".tooltip")
//                  .style("opacity", "0")
//                volumeSvg.select(".mouse-line")
//                    .style("opacity", "0")
//              })
//              .on('mouseover', function() { // on mouse in show line, circles and text
//                d3.select(".mouse-line")
//                  .style("opacity", "1");
//                d3.selectAll(".mouse-per-line circle")
//                  .style("opacity", "1");
//                d3.selectAll(".mouse-per-line text")
//                  .style("opacity", "1");
//                d3.selectAll(".tooltip")
//                  .style("opacity", "1")
//                volumeSvg.select(".mouse-line")
//                    .style("opacity", "1")
//              })
//              .on('mousemove', function() { // mouse moving over canvas
//                var mouse = d3.mouse(this);
//                d3.select(".mouse-line")
//                  .attr("d", function() {
//                    var d = "M" + mouse[0] + "," + h;
//                    d += " " + mouse[0] + "," + 0;
//                    return d;
//                  });
//                volumeSvg.select(".mouse-line")
//                  .attr("d", function() {
//                    var d = "M" + mouse[0] + "," + vh;
//                    d += " " + mouse[0] + "," + 0;
//                    return d;
//                  });
//              });
//
//              var mouseover = function(d, t) {
//                Tooltip
//                  .style("opacity", 1)
//                d3.select(t)
//                  .style("stroke", "black")
//                  .style("opacity", 1)
//              }
//              var mousemove = function(d, t) {
//                Tooltip
//                  .html("Open: " + d.open + "<br> Close: " + d.close + "<br> Volume: " + d.volume)
//                  .style("left", (d3.mouse(t)[0]+70) + "px")
//                  .style("top", (d3.mouse(t)[1]) + "px")
//              }
//              var mouseleave = function(d) {
//                Tooltip
//                  .style("opacity", 0)
//                d3.select(this)
//                  .style("stroke", "none")
//                  .style("opacity", 0.8)
//              }

                function zoomed() {

                    var t = d3.event.transform;
                    let xScaleZ = t.rescaleX(xScale);

                    let hideTicksWithoutLabel = function() {
                        d3.selectAll('.xAxis .tick text').each(function(d){
                            if(this.innerHTML === '') {
                            this.parentNode.style.display = 'none'
                            }
                        })
                    }

                    gX.call(
                        d3.axisBottom(xScaleZ)
                        .tickFormat((d, e, target) => {
                                if (d >= 0 && d <= dates.length-1) {
                            d = dates[d]
                            hours = d.getHours()
                            minutes = (d.getMinutes()<10?'0':'') + d.getMinutes() 
                            amPM = hours < 13 ? 'am' : 'pm'
                            return d.getDate() + ' ' + months[d.getMonth()] + ' ' + d.getFullYear()
                            }
                        })
                    )

                    volumeGx.call(
                        d3.axisBottom(xScaleZ)
                        .tickFormat((d, e, target) => {
                                if (d >= 0 && d <= dates.length-1) {
                            d = dates[d]
                            hours = d.getHours()
                            minutes = (d.getMinutes()<10?'0':'') + d.getMinutes() 
                            amPM = hours < 13 ? 'am' : 'pm'
                            return d.getDate() + ' ' + months[d.getMonth()] + ' ' + d.getFullYear()
                            }
                        })
                    )


                    candles.attr("x", (d, i) => xScaleZ(i) - (xBand.bandwidth()*t.k)/2)
                           .attr("width", xBand.bandwidth()*t.k)

                    stems.attr("x1", (d, i) => xScaleZ(i) - xBand.bandwidth()/2 + xBand.bandwidth()*0.5);
                    stems.attr("x2", (d, i) => xScaleZ(i) - xBand.bandwidth()/2 + xBand.bandwidth()*0.5);
                    volumeBars.attr("x", (d, i) => xScaleZ(i) - (xBand.bandwidth()*t.k)/2)
                            .attr("width", xBand.bandwidth()*t.k)

                    hideTicksWithoutLabel();

                    gX.selectAll(".tick text")
                    .call(wrap, xBand.bandwidth())

                    volumeGx.selectAll(".tick text")
                        .call(wrap, xBand.bandwidth())

                }

                function zoomend() {
                    var t = d3.event.transform;
                    let xScaleZ = t.rescaleX(xScale);
                    clearTimeout(resizeTimer)
                    resizeTimer = setTimeout(function() {

                    console.log(xScaleZ)
                    var xmin = xDateScale(Math.floor(xScaleZ.domain()[0]))
                        xmax = xDateScale(Math.floor(xScaleZ.domain()[1]))
                        filtered = _.filter(prices, d => ((d.datetime >= xmin) && (d.datetime <= xmax)))
                        minP = +d3.min(filtered, d => d.low)
                        maxP = +d3.max(filtered, d => d.high)
                        minV = +d3.min(filtered, d => d.volume)
                        maxV = +d3.max(filtered, d => d.volume)
                        buffer = Math.floor((maxP - minP) * 0.1)
                        bufferV = Math.floor((maxV - minV) * 0.1)

                    yScale.domain([minP - buffer, maxP + buffer])
                    vScale.domain([minV - buffer, maxV + buffer])
                    candles.transition()
                           .duration(800)
                           .attr("y", (d) => yScale(Math.max(d.open, d.close)))
                           .attr('height', d => (d.open === d.close) ? 1 : yScale(Math.min(d.open, d.close))-yScale(Math.max(d.open, d.close)))

                    stems.transition().duration(800)
                         .attr("y1", (d) => yScale(d.high))
                         .attr("y2", (d) => yScale(d.low))

                    volumeBars.transition()
                            .duration(800)
                            .attr("y", (d) => vScale(d.volume))
                            .attr("height", d => vh - vScale(d.volume))

                    gY.transition().duration(800).call(d3.axisLeft().scale(yScale));
                    gVY.transition().duration(800).call(d3.axisLeft().scale(vScale).ticks(6));

                    }, 500)

                }
            });
        },
        drawMovingAverage: function(symbol, direction, window) {
                d3.select("#candlestick").selectAll("#moving-average-" + direction).remove("*");
                console.log("drawing " + direction)
                d3.csv('ma/' + symbol + '/' + direction + '/' + window).then(function(avg) {
                for (var i = 0; i < avg.length; i++) {
                    avg[i]['index'] = +avg[i]['index']
                    avg[i]['close'] = parseFloat(avg[i]['close'])
                }
                console.log(avg)
                
                var line = d3.line()
                    .defined(function(d) { return !isNaN(d.close); })
                    .x(function(d) { return xScale(d.index); }) 
                    .y(function(d) { return yScale(d.close); }) 
                
                var color = (direction == 'centered') ? "#8a2be2" : "black";
                
                chartBody
                   .append("path")
                   .attr("id", "moving-average-" + direction)
                   .attr("class", "moving-average")
                   .attr("d", line(avg))
                   .attr("stroke", color)
                   .attr("stroke-width", 2)
                   .attr("fill", "none")
            });
        },
        removeMovingAverage: function(direction) {
            d3.select("#candlestick").selectAll("#moving-average-" + direction).remove("*");
        }
    }
    return newCandlesticks;
}

function wrap(text, width) {
            text.each(function() {
              var text = d3.select(this),
                  words = text.text().split(/\s+/).reverse(),
                  word,
                  line = [],
                  lineNumber = 0,
                  lineHeight = 1.1, // ems
                  y = text.attr("y"),
                  dy = parseFloat(text.attr("dy")),
                  tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
              while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                  line.pop();
                  tspan.text(line.join(" "));
                  line = [word];
                  tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
              }
            });
        }