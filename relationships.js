var relationshipsVis = function () {
    var newRelationships = {
        relationshipChart: function (nConnections, minValue) {
            
            d3.selectAll("#compare").selectAll("*").remove();
            
            console.log("running script")
            var svg = d3.select("#compare"),
            width = +svg.attr("width"),
            height = +svg.attr("height");

            var simulation = d3.forceSimulation()
                .force("link", d3.forceLink().id(function(d) { return d.id; }))
                .force("charge", d3.forceManyBody().strength(function(d) { return -10;}))
                .force("center", d3.forceCenter(width / 2 + 100, height / 2));

            var color = d3.scaleOrdinal(d3.schemePaired);
            
            var tooltip = d3.select("body")
                .append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);

            d3.json("relationships/" + nConnections + "/" + minValue).then(function(graph) {

              var link = svg.append("g")
                  .attr("class", "links")
                .selectAll("line")
                .data(graph.links)
                .enter().append("line")
                  .attr("fill", "black")
                  .style("stroke-width", function(d) { return d.value * 2})
                  .style("stroke", "black");

                console.log(graph)

              var node = svg.append("g")
                  .attr("class", "nodes")
                .selectAll("g")
                .data(graph.nodes)
                .enter()
                .append("g")
                .call(d3.drag()
                      .on("start", dragstarted)
                      .on("drag", dragged)
                      .on("end", dragended));

              var circles = node.append("circle")
                  .attr("r", 5)
                  .attr("fill", function(d) { return color(d.sector); })
                  .on('mouseover.tooltip', function(d) {
                        tooltip.transition()
                            .duration(300)
                            .style("opacity", 1);
                        tooltip.html("Symbol:" + d.id + "<p/>Sector:" + d.sector)
                            .style("left", (d3.event.pageX) + "px")
                            .style("top", (d3.event.pageY + 10) + "px");
                        })
                    .on("mouseout.tooltip", function() {
                        tooltip.transition()
                            .duration(100)
                            .style("opacity", 0);
                        })

              var legend = svg.selectAll(".legend")
                  .data(color.domain())
                .enter().append("g")
                  .attr("class", "legend")
                  .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

              legend.append("rect")
                  .attr("x", width - 18)
                  .attr("width", 18)
                  .attr("height", 18)
                  .style("fill", color);

              legend.append("text")
                  .attr("x", width - 24)
                  .attr("y", 9)
                  .attr("dy", ".35em")
                  .style("text-anchor", "end")
                  .text(function(d) { 
                    console.log(d);
                    return d; });

              node.append("title")
                  .text(function(d) { return d.id; });

              simulation
                  .nodes(graph.nodes)
                  .on("tick", ticked);

              simulation.force("link")
                  .links(graph.links);

              function ticked() {
                link
                    .attr("x1", function(d) { return d.source.x; })
                    .attr("y1", function(d) { return d.source.y; })
                    .attr("x2", function(d) { return d.target.x; })
                    .attr("y2", function(d) { return d.target.y; });

                node
                    .attr("transform", function(d) {
                      return "translate(" + d.x + "," + d.y + ")";
                    })
              }
            });

            function dragstarted(d) {
              if (!d3.event.active) simulation.alphaTarget(0.3).restart();
              d.fx = d.x;
              d.fy = d.y;
            }

            function dragged(d) {
              d.fx = d3.event.x;
              d.fy = d3.event.y;
            }

            function dragended(d) {
              if (!d3.event.active) simulation.alphaTarget(0);
              d.fx = null;
              d.fy = null;
            }
        }
    }
    
    return newRelationships;
}

