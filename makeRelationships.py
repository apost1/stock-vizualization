from os import listdir
import json
import pandas as pd

data = dict()
nodes = dict()
nodes_lst = []
links = dict()
links_lst = []
relationships_dict = dict()

sectors = pd.read_csv("constituents_csv.csv")
sectors_dict = dict()

for index, row in sectors.iterrows():
    sectors_dict[row.Symbol] = row.Sector

def getRelationships(name, df):
    resultsList = []
    for file in listdir("data"):
        if (file.endswith(".csv")):
            results = dict()
            newName = file.split(".csv")[0]
            if (newName != name):
                newFile = pd.read_csv("data/" + file)
                diff = df.diff()
                newDiff = newFile.diff()
                close = diff.dropna()['close']
                newClose = newDiff.dropna()['close']
                corr = close.corr(newClose)
                results['source'] = name
                results['target'] = newName
                results['value'] = corr
                resultsList.append(results)
    
    sortedList = sorted(resultsList, key = lambda v: v['value'], reverse=True)
    relationships_dict[name] = sortedList[:25]
    return sortedList[:25]

with open('sectors.json', 'w') as fp:
    json.dump(sectors_dict, fp)



for file in listdir("data"):
    if file.endswith(".csv"):
        name = file.split(".csv")[0]
        obj = dict()
        obj['id'] = name
        if name in sectors_dict.keys():
            obj['sector'] = sectors_dict[name]
        else:
            obj['sector'] = 'Unknown'
        nodes_lst.append(obj)
        df = pd.read_csv("data/" + file)
        relationships = getRelationships(name, df)
        for x in relationships:
            links_lst.append(x)

with open('symbol_relationships.json', 'w') as fp:
    json.dump(relationships_dict, fp)

data['nodes'] = nodes_lst
data['links'] = links_lst

import json

with open('relationships.json', 'w') as fp:
    json.dump(data, fp)
        
        