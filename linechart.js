var linechartVis = function(){
    var lineChart = null;
    var x = null;
    var y = null;
    var newLineChart = {
        drawLineChart: function(stock){
            d3.csv("data/" + stock).then(function(data) {

                for (var i = 0; i < data.length; i++) {
                    data[i].open = +data[i].open;
                    data[i].close = +data[i].close;
                    data[i].high = +data[i].high;
                    data[i].low = +data[i].low;
                    data[i].volume = +data[i].volume;
                    data[i].avg = (data[i].open + data[i].close)/2;
                    data[i].datetime = new Date(+data[i].datetime);}
                
                console.log(data);
                var margin = {top: 20, right: 20, bottom: 110, left: 40};
                var margin2 = {top: 430, right: 20, bottom: 30, left: 40};
                width = 960
                height = 500
        
                d3.selectAll("svg").remove();
        
                d3.select(".card-body-area")
                    .append("svg")
                    .attr("class", "chart linechart")
                    .attr("width", width)
                    .attr("height", height);
        
        
                var svg = d3.select(".linechart"),
                    width = +svg.attr("width") - margin.left - margin.right,
                    height = +svg.attr("height") - margin.top - margin.bottom,
                    height2 = +svg.attr("height") - margin2.top - margin2.bottom;
        
                x = d3.scaleTime()
                            .domain(d3.extent(data, function(d) { return d.datetime; }))
                            .range([0, width]);
                y = d3.scaleLinear()
                            .domain([0, d3.max(data, function(d) { return d.avg})])
                            .range([height, 0]);
        
                var x2 = d3.scaleTime()
                            .domain(x.domain())
                            .range([0, width]);
                var y2 = d3.scaleLinear()
                            .domain(y.domain())
                            .range([height2, 0]);
                
                var thick = d3.scaleLinear()
                            .domain(d3.extent(data, function (d) { return d.volume; }))
                            .range([2, 10]);


                var formatyAxis = d3.format('.1f');
        
                var xAxis = d3.axisBottom(x),
                    yAxis = d3.axisLeft(y).tickFormat(formatyAxis);
                var xAxis2 = d3.axisBottom(x2);
        
                // set zoom and brush objects
                var zoom = d3.zoom()
                            .scaleExtent([1,Infinity])
                            .translateExtent([[0,0],[width, height]])
                            .extent([[0,0], [width, height]])
                            .on("zoom", zoomed);
        
                var brush = d3.brushX()
                            .extent([[0, 0], [width, height2]])
                            .on("brush end", brushed);
        
                // set the clip during a zoom in 
                var clip = svg.append("defs")
                            .append("svg:clipPath")
                                .attr("id","clip")
                                .append("svg:rect")
                                    .attr("width",width)
                                    .attr("height",height)
                                    .attr("x",0)
                                    .attr("y",0);
        
                // define a line
                // var line = d3.line()
                //             .x(function(d) {return x(d.datetime);})
                //             .y(function(d) {return y(d.avg);});
                //             //.interpolate("basis");

                var line2 = d3.line()
                            .x(function (d) { return x2(d.datetime); })
                            .y(function (d) { return y2(d.avg); });
        
        
                lineChart = svg.append("g")
                                    .attr("class","focus")
                                    // start the line chart by moving it the margin amount
                                    .attr("transform","translate(" + margin.left + "," + margin.top + ")")
                                    .attr("clip-path","url(#clip)")
        
                // var line = d3.line()
                //             .x(function(d) {return x(d.datetime);})
                //             .y(function(d) {return y(d.avg);});
                //             //.interpolate("basis");
                // lineChart.append("path")
                //             .datum(data)
                //             .attr("class","line")
                //             .attr("d",line);
                lineChart.selectAll("line")
                            .data(data)
                            .enter()
                            .append("line")
                            .attr("class", "line")
                            .attr("x1", function(d,i){ 
                                if (i === 0) { return x(d.datetime);
                                } else if (i > 0) { return x(data[i-1]["datetime"]); }
                            })
                            .attr("x2", function (d, i) {
                                if (i === 0) { return x(d.datetime);
                                } else if (i > 0) { return x(d.datetime); }
                            })                            
                            .attr("y1", function (d, i) {
                                if (i === 0) { return y(d.avg);
                                } else if (i > 0) { return y(data[i - 1]["avg"]); }
                            })                            
                            .attr("y2", function (d, i) {
                                if (i === 0) { return y(d.avg);
                                } else if (i > 0) { return y(d.avg); }
                            })                            
                            .attr("stroke-width", function (d, i) { return thick(d.volume)});
                                
                var SVGfocus = svg.append("g")
                                .attr("class", "focus")
                                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
                SVGfocus.append("g")
                        .attr("class", "axis axis--x")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);
        
                SVGfocus.append("g")
                        .attr("class", "axis axis--y")
                        .call(yAxis);            
        
                var context = svg.append("g")
                    .attr("class", "context")
                    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
        
                context.append("path")
                    .datum(data)
                    .attr("class", "line")
                    .attr("d", line2);
        
                context.append("g")
                    .attr("class", "axis axis--x")
                    .attr("transform", "translate(0," + height2 + ")")
                    .call(xAxis2);
        
                context.append("g")
                    .attr("class", "brush")
                    .call(brush)
                    .call(brush.move, x.range());
        
                svg.append("rect")
                    .attr("class","zoom")
                    .attr("width", width)
                    .attr("height", height)
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .call(zoom);    
        
                function brushed() {
                    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
                    var s = d3.event.selection || x2.range();
                    x.domain(s.map(x2.invert, x2));

                    var min_x = x.domain()[0],
                        max_x = x.domain()[1];

                    var dataSub = []
                    for (i = 0; i < data.length; i++) {
                        if (data[i]["datetime"] >= min_x && data[i]["datetime"] <= max_x) {
                            dataSub.push(data[i]);
                        };
                    };
                    var thick = d3.scaleLinear()
                        .domain(d3.extent(dataSub, function (d) { return d.volume; }))
                        .range([2, 10]);

                    lineChart.selectAll("line")
                        .data(dataSub)
                        .enter()
                        .append("line")
                        .attr("class", "line")
                        .attr("x1", function (d, i) {
                            if (i === 0) {
                                return x(d.datetime);
                            } else if (i > 0) { return x(dataSub[i - 1]["datetime"]); }
                        })
                        .attr("x2", function (d, i) {
                            if (i === 0) {
                                return x(d.datetime);
                            } else if (i > 0) { return x(d.datetime); }
                        })
                        .attr("y1", function (d, i) {
                            if (i === 0) {
                                return y(d.avg);
                            } else if (i > 0) { return y(dataSub[i - 1]["avg"]); }
                        })
                        .attr("y2", function (d, i) {
                            if (i === 0) {
                                return y(d.avg);
                            } else if (i > 0) { return y(d.avg); }
                        })
                        .attr("stroke-width", function (d, i) { return thick(d.volume) });

                    //lineChart.select(".line").attr("d", line);
                    SVGfocus.select(".axis--x").call(xAxis);
                    svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
                        .scale(width / (s[1] - s[0]))
                        .translate(-s[0], 0));};
        
                function zoomed() {
                    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
                    var t = d3.event.transform;
                    var new_xScale = t.rescaleX(x);
                    var new_yScale = t.rescaleY(y);

                    // get new max and min dates based on zoom and subset the data
                    var min_x = new_xScale.domain()[0],
                        max_x = new_xScale.domain()[1];

                    var dataSub = []
                    for (i = 0; i < data.length; i++) {
                        if (data[i]["datetime"] >= min_x && data[i]["datetime"] <= max_x){
                            dataSub.push(data[i]);
                        };
                    };

                    var thick = d3.scaleLinear()
                        .domain(d3.extent(dataSub, function (d) { return d.volume; }))
                        .range([2, 10]);

                    //console.log(dataSub);
        
                    // var data_sub = {}
                    // for data.
                   
                    //var new_thick = 
                    // remove old lines
                    d3.selectAll(".line").remove()
                    lineChart.selectAll("line")
                        .data(dataSub)
                        .enter()
                        .append("line")
                        .attr("class", "line")
                        .attr("x1", function (d, i) {
                            if (i === 0) {
                                return new_xScale(d.datetime);
                            } else if (i > 0) { return new_xScale(dataSub[i - 1]["datetime"]); }
                        })
                        .attr("x2", function (d, i) {
                            if (i === 0) {
                                return new_xScale(d.datetime);
                            } else if (i > 0) { return new_xScale(d.datetime); }
                        })
                        .attr("y1", function (d, i) {
                            if (i === 0) {
                                return new_yScale(d.avg);
                            } else if (i > 0) { return new_yScale(dataSub[i - 1]["avg"]); }
                        })
                        .attr("y2", function (d, i) {
                            if (i === 0) {
                                return new_yScale(d.avg);
                            } else if (i > 0) { return new_yScale(d.avg); }
                        })
                        .attr("stroke-width", function (d, i) { return thick(d.volume) });

                    
                    x.domain(t.rescaleX(x2).domain());
                    //y.domain(new_yScale);          
                    //lineChart.select(".line").attr("d", line);
                    SVGfocus.select(".axis--x").call(xAxis.scale(new_xScale));
                    SVGfocus.select(".axis--y").call(yAxis.scale(new_yScale));
                    context.select(".brush").call(brush.move, x.range().map(t.invertX, t));};
            });
        
        },
        drawMovingAverage: function(symbol, direction, window) {
                d3.select("#linechart").selectAll("#moving-average-" + direction).remove("*");
                console.log("drawing " + direction)
                d3.csv('ma/' + symbol + '/' + direction + '/' + window).then(function(avg) {
                for (var i = 0; i < avg.length; i++) {
                    avg[i]['index'] = +avg[i]['index']
                    avg[i]['close'] = parseFloat(avg[i]['close'])
                    avg[i].datetime = new Date(+avg[i].datetime);
                }
                    
                console.log(symbol + direction + window);
                var line = d3.line()
                    .defined(function(d) { return !isNaN(d.close); })
                    .x(function(d) { return x(d.datetime); }) 
                    .y(function(d) { return y(d.close); }) 
                
                var color = (direction == 'centered') ? "#8a2be2" : "black";
                
                lineChart
                   .append("path")
                   .attr("id", "moving-average-" + direction)
                   .attr("class", "moving-average")
                   .attr("d", line(avg))
                   .attr("stroke", color)
                   .attr("stroke-width", 2)
                   .attr("fill", "none")
            });
        },
        removeMovingAverage: function(direction) {
            d3.select("#linechart").selectAll("#moving-average-" + direction).remove("*");
        }

    }

    return newLineChart;

};

// function draw_linechart(stock){ 
    
// };