from flask import Flask, send_from_directory, render_template
import csv
import random
import json
import pandas as pd
from os import listdir
from os.path import isfile, join
from flask import jsonify


app = Flask(__name__)

# @app.route('/')
# def index():
#     return render_template('index.html')

@app.route('/<path:path>')
def startup(path):
    return send_from_directory('.', path)

@app.route('/data/companies')
def companies():
    onlyfiles = [f for f in listdir('data') if isfile(join('data', f))]
    df = pd.read_csv('companylist.csv')
    data = [x.split('.csv')[0] for x in onlyfiles]
    companies = []
    for index, row in df.iterrows():
        if row.Symbol in data:
            obj = {}
            obj['value'] = row.Symbol
            obj['label'] = row.Symbol
            obj['desc'] = row.Name
            companies.append(obj)
    return jsonify(companies)

@app.route('/data/<string:symbol>')
def data(symbol):
    return send_from_directory('data', symbol + '.csv')

@app.route("/ma/<string:symbol>/<string:direction>/<int:window>")
def movingAverage(symbol, direction, window):
    df = pd.read_csv('data/' + symbol + '.csv')
    if direction == 'centered':
        newDf = df['close'].rolling(window, center=True).mean()
    else:
        newDf = df['close'].rolling(window, center=False).mean()
    
    result = pd.DataFrame(newDf, columns=['close'])
    result['datetime'] = df.datetime;
    result.index.name = 'index'
    
    return result.to_csv()
        
@app.route("/relationships/<int:lim>/<float:minSim>")
def getRelationships(lim, minSim):
    sectors = dict()
    symbol_relationships = dict()
    with open('sectors.json') as f:
        sectors = json.load(f)
    with open('symbol_relationships.json') as f:
        symbol_relationships = json.load(f)
    nodes_lst = []
    links_lst = []
    data = dict()
    for file in listdir("data"):
        if file.endswith(".csv"):
            name = file.split(".csv")[0]
            obj = dict()
            obj['id'] = name
            if name in sectors.keys():
                obj['sector'] = sectors[name]
            else:
                obj['sector'] = 'Unknown'
            nodes_lst.append(obj)
            rel = symbol_relationships[name]
            for i, x in enumerate(rel):
                if i < lim and x['value'] > minSim:
                    links_lst.append(x)
                    
    data['nodes'] = nodes_lst
    data['links'] = links_lst
    
    return jsonify(data)
    
            

if __name__ == '__main__':
    app.run(debug=True)
        
    
